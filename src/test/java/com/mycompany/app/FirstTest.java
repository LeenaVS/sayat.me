package com.mycompany.app;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;


public class FirstTest
{
  @Test
    public void LogInFeedback() {
      WebDriver driver = new HtmlUnitDriver();

        driver.get("http://sayat.me/");
        //Logi süsteemi sisse
        WebElement loginElement = driver.findElement((By.className("login")));
        WebElement loginElement2 = loginElement.findElement(By.className("toggle"));
        loginElement2.click();
        driver.findElement(By.id("fburl_d")).clear();
        driver.findElement(By.id("fburl_d")).sendKeys("LeenaSimm");
        driver.findElement(By.id("login_passwd_d")).clear();
        driver.findElement(By.id("login_passwd_d")).sendKeys("LeenaSimm1");
        driver.findElement(By.xpath("(//button[@type='submit'])[3]")).click();

        //Avan teise kasutaja tagasiside vormi ja sisestan tagasiside
        driver.get("http://sayat.me/leenaTest");
        driver.findElement((By.className("give-feedback"))).findElement(By.tagName("textarea")).sendKeys("oled tore inimene");
        driver.findElement((By.className("give-feedback"))).findElement(By.tagName("textarea")).submit();

        // kontrollin, kas tagasiside on salvestatud
        //  assertEquals("oled tore inimene", driver.findElement(By.cssSelector("p.feed-item")).getText());

        // login välja
        driver.findElement(By.linkText("Leena")).click();
        driver.findElement(By.linkText("Logi välja")).click();
    }
}
